@extends('layouts.app')

@section('content')
<li class="nav-item">
    <a class="nav-link active" href="{{ url('/random') }}">Tire une idée au hasard</a>
    </li>
<div class="container">
  <form method="GET">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Rentre ta super idée') }}</div>
            <input type="text" name="idée">
            <button type="submit" class="btn btn-primary">Envoie la</button>   
            </div>
        </div>
    </div>
 </form> 
</div>
@endsection
