@extends('layouts.app')

@section('content')

<div class="container">
  <form method="GET">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __("Saisi l'endroit et la date de l'événement") }}</div>
            <input type="text" name="location">
            <input type="date" min="{{ date('Y-m-d') }}" name="date">
            <button type="submit" class="btn btn-primary">Envoyer</button>   
            </div>
        </div>
    </div>
 </form> 
</div>
</div>

<div class='container'>
<div class='row'>
<div class='container col-md-6'>
<h3> Liste des adhérents </h3>
<ul class="list-group list-group-vertical">
@foreach($users as $user)
  <li class="list-group-item">{{ $user['name'] }}  {{$user['email'] }}</li>
@endforeach
</ul>
</div>


<div class='container col-md-6'>
<h3> Liste des événements </h3>
<ul class="list-group list-group-vertical">
@foreach($events as $event)

<li class="list-group-item">{{ $event['location'] }} le {{$event['date'] }}</li>
@endforeach
</ul>
</div>
</div>
</div>

@endsection