<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@list')->name('home')->middleware('auth');

Route::get('/random', 'RandomController@index')->name('random')->middleware('auth');

Route::get('/evenement', 'EvenementController@index')->name('evenement')->middleware('auth');

