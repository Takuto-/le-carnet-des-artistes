<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sujet;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function list(Request $request)
    {
        $idée = $request->input('idée');
        if($idée != "") {
        $sujet = Sujet::create([
            'content' => "$idée",
        ]);
        }
        return view('/home');
    }
}

 // $sujet = new Sujet;
        // $sujet->content = $request->$idée;
        // $sujet->save();