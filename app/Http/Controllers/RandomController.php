<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sujet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class RandomController extends Controller
{
    public function index(Request $request) {
        $sujet = Sujet::select('content')->inRandomOrder()->first();
        // dd($sujet);
        
        if($sujet != "") {
        DB::table('sujets')->where('content','=',$sujet['content'])->delete(); 
        return view('random',[
            'sujet' => $sujet
        ]); }
        
        
    
    }
     

}


// if ($sujet === "") {
//     return view('random',[
//         'nomore' => "il n'y a plus de sujets"
//     ]);
// }